FROM node:14.3.0

#Crear nuevo usuario y Home Dir
RUN useradd user-node -d /home/user-node
RUN mkdir /home/user-node
RUN chown user-node.user-node /home/user-node

#Crear directorio
RUN mkdir /opt/app
RUN chown user-node.user-node /opt/app
USER user-node

#Copiar APP e instalar dependencias
COPY . /opt/app
WORKDIR /opt/app
RUN npm install
RUN npm run test
